# Empresa Estudio Project

# Requirements:

1. Create the above UI structure.
2. Implement a responsive design for the page (4 rooms on desktop, 3 on tablets, 2 on mobile, and 1 on extra small devices)
3. Create an appropriate JSON for the rooms.
4. Create an Ajax request to get the JSON file.
5. Create a task to compile the LESS file into CSS and watch for changes.
6. Create a task for compressing and combining all of your css files
7. Create a package.json file for your needed dependencies.
8. Create a filter for the rooms to filter the rooms based on the id or style: when performing a filter action, if the customer refreshes the page, the filter action must be saved (don't use any type of local storage or cookies)
9. Update the readme file with the steps needed to install & run this project locally. 

# Design considerations:
- Make sure that your code supports both BS V3 & BS V4
- All styles should be written in LESS format.
- Install the needed LESS compiler using npm modules.
- The page should scroll to display all the rooms.
- The header is fixed while scrolling.
- The website logo should be created using CSS (not an image)
- Clicking on the image should display the full image in a modal.
- Hovering over the image should animate the title from the bottom and enlarge the image.- Use the same colors from the image.

# Development considerations:
- You should have 7 branches for the above requirements.
- Start the requirements in the same order.
- Use Jquery in your code.
- Use success and fail functions in your Ajax
- and handle request timed out.
- Your code should handle any number of rooms, so if I added more rooms the page should load up the old and new ones.